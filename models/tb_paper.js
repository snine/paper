'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tb_paper extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  tb_paper.init({
    PaperName: DataTypes.STRING,
    PaperUrl: DataTypes.STRING,
    CoverImg: DataTypes.STRING,
    TagId: DataTypes.BIGINT,
    TagName: DataTypes.STRING,
    DetailImg: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'tb_paper',
  });
  return tb_paper;
};