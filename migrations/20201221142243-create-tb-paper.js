'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tb_papers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      PaperName: {
        type: Sequelize.STRING
      },
      PaperUrl: {
        type: Sequelize.STRING
      },
      CoverImg: {
        type: Sequelize.STRING
      },
      DetailImg: {
        type: Sequelize.STRING
      },
      TagId: {
        type: Sequelize.BIGINT
      },
      TagName: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tb_papers');
  }
};