const models = require('../../models')

exports.addTag = async(data) => {
    const result = await models.tb_tag.bulkCreate(data)
    return result;
}

exports.addPaper = async(data) => {
    const result = await models.tb_paper.bulkCreate(data)
    // console.log(result);
}