const cheerio = require('cheerio')
const https = require('https')
const http = require('http')
const fs = require('fs')

const {addTag,addPaper} = require('./service/index')

const tagNum = 21;  //需要第几个标签
const coverImgPage = 5; //需要多少页的封面图片
const detailImgNum = 9; //一组图片需要多少页的详情图

/** 默认使用http请求方式，如果是https，第二个参数传个ture就行 封装的请求方法 */
const nodeSpider = (url, https = false) => {
    return new Promise((resovle, reject) => {
        const request = https ? https : http
        request.get(url, async res => {
            let html = ''
            res.on('data', data => html += data)
            res.on('end', async () => {
                let $ = cheerio.load(html, { decodeEntities: false })
                resovle($)
            }),
                res.on('err', err => reject(err))
        })
    })
}

/** 获取所有的标签并让所有连接指向图片大小为1080*1920的网页地址 */
const getTags = async (url) => {
    const $ = await nodeSpider(url)
    const tagListNodes = $('.cont1').find('a').nextAll()
    let TagList = []
    tagListNodes.each((index, node) => {
        let TagName = $(node).html()
        let TagId = Number(index + 1)
        let oldHref = $(node).attr('href')
        let newHref = oldHref.replace('_0_0_1.html', '_0_119_1.html') //这样可以把图片大小固定为1080*1920
        TagList.push({ TagName, href: newHref, TagId })
    })
    return [TagList[tagNum]];
}

/** 获取所有封面图片 */
const getCoverImgs = async (url) => {
    let coverImgList = []
    for (let i = 0; i < coverImgPage; i++) { //这里是一个分类下的一页，最多有五页 i
        const path = url.replace('1.html',`${i+1}.html`)
        const $ = await nodeSpider(path)
        const coverImgNodes = $('.tab_box').find('.clearfix').find('li')
        coverImgNodes.each((index, node) => {
            const path = $(node).find('a').attr('href')  //通过这个地址去到该图片的详细界面
            const CoverImg = $(node).find('a').find('img').attr('data-src')  //该网页是懒加载的，需要这个属性去拿真正的图片
            const PaperName = $(node).find('a').find('img').attr('alt') //这就是封面图片，压缩过的，不清晰图
            coverImgList.push({ path, CoverImg, PaperName })
        })
    }
    console.log('coverImgList',coverImgList);
    return coverImgList
}

/** 从封面图片的路径进去找到关于这个封面更多的图片 */
const getDetailImgs = async (url) => {
    let detailImgs = []
    for (let i = 0; i < detailImgNum; i++) { //这个i表示一组图片需要几张最多8张图片，我们需要拿到几张
        const path = url.replace('.html', `_${i + 1}.html`)
        const $ = await nodeSpider(path)
        const detailUrl = $('.pic-meinv').find('a').find('.pic-large').attr('src')
        if (detailUrl){
            detailImgs.push(detailUrl)
        };
    }
    return detailImgs;
}

/** 把数据写入到json文件里面 */
const writeFile = (data) => {
    let oldData = fs.readFileSync('./data.json', 'utf-8')
    if (oldData) {
        console.log('有之前的数据');
        oldData = JSON.parse(oldData)
    } else {
        oldData = []
    }
    let newData = []
    oldData ? newData = [...oldData, ...data] : newData = data
    fs.writeFileSync('./data.json', JSON.stringify(newData), (err) => {
        if (err) console.log(err);
        console.log('写入成功');
    })
}

/** 获取分类的封面 */
const getCoverImg = async(url) => {
    const $ = await nodeSpider(url)
    let coverImg = $('.tab_box').find('ul').find('li').find('a').find('img').attr('data-src')
    return coverImg
}

/** 获取分类加分类的封面 */
const getTagList = async(url) => {
    let TagList = await getTags(url)
    let task = []
    TagList.forEach( (item,index) => {
        task.push(getCoverImg(item.href))
    })
    let allCoverImg = await Promise.all(task)
    TagList = TagList.map( (t,i) => {
        t.CoverImg = allCoverImg[i]
        return t;
    })
    return TagList;
}
const url = 'http://www.win4000.com/mobile.html'





/** 获取标签并存入数据库 */
// getTagList(url).then(res => {
//     addTag(res)
// })

return;
getTagList(url).then(async res => {
    // res => 拿到标签列表路径 默认是数组 ，但是数据量太大， 默认每次传一个标签 修改tagNum代表第几个标签 
    // 单次爬取网页数量 = 1个标签 * 24组图片 * 5页图片 * 9张每组图片
    res.forEach( async(item,index) => {
        const allZuImg = await getCoverImgs(item.href) //拿到一个标签五页共120组照片 有封面可以查看详情页的图片 通过path可以去到详情页拿到高清原图
        let task = []   // 异步任务先建立一个task的栈储存所有异步任务
        allZuImg.forEach( async(t,i) => {
            task.push(getDetailImgs(t.path))
        })
        const allDetailImg = await Promise.all(task) //拿到所有的返回值 这个代表一个path每组图片的path去详情页里面拿了9张高清图 9张图不是一个页面的也是九页的 所以注意性能

        let resultData = allZuImg.map( (k,j) => {
            delete item['path']
            k.TagName = item.TagName
            k.TagName = item.TagName
            k.TagId = item.TagId
            k.PaperUrl = k.CoverImg
            k.DetailImg = allDetailImg[j]
            return k
        }) //这一步是给一组图片单独打标 加上分类的名称id

        /** 整合到数据库去  不要path字段 把DetailImg字段给序列化 */
        let res = []
        resultData.forEach ( (t,i) => {
            letobj =  {
                PaperName: t.PaperName,
                PaperUrl: t.PaperUrl,
                CoverImg: t.CoverImg,
                TagId: t.TagId,
                TagName: t.TagName,
                DetailImg: JSON.stringify(t.DetailImg),
            }
            res.push(letobj)
        })

        // 写入到数据库即可
        // writeFile(res) //可以写入到同级目录data.json文件之中
    })
})



