const express = require('express')
const router = express.Router()
const models = require('../../models')
const Result = require('../../utils/Result')

/**
 *  @Router get /paper/query
 *  @desc   查询图片接口
 *  @access 接口是公开的
 */
router.get('/query', async (req, res) => {
    let { TagId, currentPage = 1, pageSize = 10, keyword} = req.query
    let where = {}
    TagId ? where.TagId = TagId : ''
    keyword ? where.PaperName = {[Op.like]: '%' + keyword + '%'} : ''
    const result = await models.tb_paper.findAndCountAll({
        order: [['id', 'DESC']], //倒叙的方式输出 对比id 默认为ASC正序
        where,
        offset: (currentPage - 1) * pageSize,
        limit: pageSize,
    })
    result.rows = result.rows.map( t => {
        t.DetailImg = JSON.parse(t.DetailImg)
        return t;
    })
    new Result(result, 'success').success(res)
})


/**
 *  @Router get /paper/random
 *  @desc   查询一组随机图片
 *  @access 接口是公开的
 */
router.get('/random', async (req, res) => {
    const allNum = await models.tb_paper.count()
    const id = sum(1, allNum)
    const img = await models.tb_paper.findByPk(id)
    img.DetailImg = JSON.parse(img.DetailImg)
    new Result(img, 'success').success(res)
})

module.exports = router

/** 取一个随机数 */
function sum(m, n) {
    var num = Math.floor(Math.random() * (m - n) + n);
    return num
}