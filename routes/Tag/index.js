const express = require('express')
const router = express.Router()
const models = require('../../models')
const Result = require('../../utils/Result')

router.get('/query', async (req, res) => {

    let {currentPage = 1, pageSize = 10} = req.query
    const result = await models.tb_tag.findAndCountAll({
        order: [['id', 'DESC']], //倒叙的方式输出 对比id 默认为ASC正序
        offset: (currentPage - 1) * pageSize,
        limit: pageSize,
    })

    new Result(result, 'success').success(res)
})


module.exports = router